#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QLabel>
#include <QMenu>
#include <QXmlStreamReader>

class MainWindow : public QLabel
{
    Q_OBJECT

public:
    MainWindow(QLabel *parent = 0);
    bool xmlMenus();
    void parseMenu(QXmlStreamReader &xml);
public slots:
    void ctxMenu(const QPoint &pos);
    void onMenuClick();
    void mouseMoveEvent(QMouseEvent *ev);
};

#endif // MAINWINDOW_H
