#include "mainwindow.h"
#include <iostream>
#include <QCoreApplication>
#include <QMouseEvent>
#include <QTransform>
#include <QVBoxLayout>
#include <QLabel>
#include <QMenu>
#include <QFile>
#include <QIODevice>
#include <QXmlStreamReader>
#include <QTextStream>
#include <QXmlStreamReader>
#include "assets.h"

using namespace std;

QPoint globalPos;
QXmlStreamReader xml;
QMenu *menu, *submenu;
using namespace std;

MainWindow::MainWindow(QLabel *)
{
    setMouseTracking(true);
    QImage myImage;
    menu = new QMenu;
    myImage.load(MAIN_IMAGE_PATH);
    setPixmap(QPixmap::fromImage(myImage));
    setContextMenuPolicy(Qt::CustomContextMenu);
    connect(this, SIGNAL(customContextMenuRequested(const QPoint &)), this, SLOT(ctxMenu(const QPoint &)));
    xmlMenus();
}

void MainWindow::mouseMoveEvent(QMouseEvent *ev) {
   QTransform t;
   t.scale(1, -1);
   t.translate(0, -height()+1);
   globalPos = ev->pos() * t;
}

void MainWindow::ctxMenu(const QPoint &pos) {
    menu->exec(this->mapToGlobal(pos));
}

void MainWindow::onMenuClick()
{
    QString data = QString("%1, %2").arg(globalPos.x()).arg(globalPos.y());
    QFile file(DATA_FILE_PATH);
    if (file.open(QIODevice::Append)) {
        QTextStream stream(&file);
        stream << data << endl;
    }
}

void MainWindow::parseMenu(QXmlStreamReader &xml) {
    QIcon *icon;
    QString id;
    QString name, iconPath;
    xml.readNext();
    while (!(xml.tokenType() == QXmlStreamReader::EndElement && xml.name() == "menu"))
    {
        if (xml.tokenType() == QXmlStreamReader::StartElement)
        {
            if (xml.name() == "id")
                id = xml.readElementText();
            if (xml.name() == "caption")
                name = xml.readElementText();
            if (xml.name() == "icon")
                iconPath = xml.readElementText();
        }
        xml.readNext();
    }
    // Relative path to the icons from XML
    icon = new QIcon(RELATIVE_ASSETS_PATH + iconPath);
    submenu->addAction(*icon, name, this, SLOT(onMenuClick()));
    submenu->setStyleSheet("QMenu::item { height: 40px; min-width: 140px; margin: 10px; }");

    QFont font = submenu->font();
    font.setBold(true);
    font.setPointSize(15);
    submenu->setFont(font);
}

bool MainWindow::xmlMenus() {
    QIcon *icon = new QIcon(MENU_ICON_PATH);
    submenu = menu->addMenu(*icon, "First menu");

    QFile* file = new QFile(MENU_PATH);

    if (!file->open(QIODevice::ReadOnly | QIODevice::Text))
    {
        return false;
    }
    QXmlStreamReader xml(file);
    while (!xml.atEnd() && !xml.hasError())
      {
          QXmlStreamReader::TokenType token = xml.readNext();
          if (token == QXmlStreamReader::StartDocument)
              continue;
          if (token == QXmlStreamReader::StartElement)
          {
              if (xml.name() == "menus")
                  continue;
              if (xml.name() == "menu")
                  parseMenu(xml);
          }
    }

    QFont font = menu->font();
    font.setBold(true);
    font.setPointSize(15);
    menu->setFont(font);
    menu->setStyleSheet("QMenu::item { height: 40px; min-width: 140px; margin: 10px; padding-left:20px}");
    return false;
}
