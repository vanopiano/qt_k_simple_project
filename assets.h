#ifndef ASSETS
#define ASSETS

#ifdef QT_DEBUG
    // In the debug build needs to put assets files near the project file
    #ifdef Q_OS_DARWIN
        #define RELATIVE_ASSETS_PATH QCoreApplication::applicationDirPath() + "/../../../../"
    #else
        #define RELATIVE_ASSETS_PATH QCoreApplication::applicationDirPath() + "/../"
    #endif
#else
    // In the release build needs to put assets files near the executable file
    #ifdef Q_OS_DARWIN
        #define RELATIVE_ASSETS_PATH QCoreApplication::applicationDirPath() + "/../../../"
    #else
        #define RELATIVE_ASSETS_PATH QCoreApplication::applicationDirPath() + "/"
    #endif
#endif

#define MAIN_IMAGE_PATH RELATIVE_ASSETS_PATH + "image.jpg"
#define DATA_FILE_PATH RELATIVE_ASSETS_PATH + "data.txt"
#define MENU_ICON_PATH RELATIVE_ASSETS_PATH + "icon.png"
#define MENU_PATH RELATIVE_ASSETS_PATH + "menu.xml"

#endif // ASSETS
